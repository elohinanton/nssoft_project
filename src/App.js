import React, { Component } from 'react';
// import logo from './logo.svg';
import './App.css';
import DataTable from './Components/DataTable/DataTable'


class App extends Component {
  render() {
    return (
      <div className="App">
        <DataTable />
      </div>
    );
  }
}

export default App;
