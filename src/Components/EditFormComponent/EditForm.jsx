import React, { Component } from 'react';

export default class EditForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            formData: {
                Name: "",
                Age: "",
                Email: "",
                Phone: ""
            },
            cardId: "",
            cardStatus: ""
        };
    }

    setFieldValue = (key) => {
        return (e) => {
            this.setState({
                ...this.state,
                formData: {
                    ...this.state.formData,
                    [key]: e.target.value
                }
            })
        }
    }

    cancelForm = () => {
        this.setState(state => {
            const formData = {};
            Object.keys(state.formData).forEach(field => formData[field] = "");
            return {...state, formData }
        }, () => this.props.setActiveStatus(false))
    }

    CreateOnTheServer = () => {
        const formData = this.createRequestObject();
        fetch(this.props.API + "/api/records", {
            method: 'PUT',
            body: formData
        }).then(response => response.json())
        // .then(response => console.log(response))
        .catch(error => console.log("not create on server", error));
        this.cancelForm();
    }

    EditOnTheServer = () => {
        this.cancelForm();
    }

    createRequestObject = () => {
        let myResponse = {
            data: {
                ...this.state.formData
            }
        }
        return myResponse
    }

    render() {
        const { formData } = this.state;
        const { setFieldValue, cancelForm, CreateOnTheServer, EditOnTheServer } = this;

        return <div>
            <div
                className="edit_form__inputs"
                style={{
                    display: "flex",
                    justifyContent: 'center',
                    alignItems: 'flex-end',
                    flexDirection: 'column'
            }}>
                {
                    Object.entries(formData).map(([key, value]) => {
                        return (<label>{ key }<input
                            type="text"
                            value={value}
                            onChange={setFieldValue(key)}
                        /></label>)
                    })
                }
            </div>
            <div style={{
                display: "flex",
                justifyContent:"space-evenly",
                margin:".5em"
            }}>
                <button onClick={() => cancelForm()}>Отмена</button>
                <button
                    onClick={() => {
                        if (this.props.operationType === "add"){
                            CreateOnTheServer()
                        } else if (this.props.operationType === "edit"){
                            EditOnTheServer()
                        }
                    }}
                >
                    {
                        (this.props.operationType === "add") ? "Добавить" :
                            (this.props.operationType === "edit") ? "Изменить" :
                                "wtf?"
                    }
                </button>
            </div>
        </div>
    }
}