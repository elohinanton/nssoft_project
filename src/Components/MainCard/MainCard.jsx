import React, { Component } from 'react';
import './MainCard.sass'
import EditForm from "../EditFormComponent/EditForm";


export default class MainCard extends Component {

    constructor(props) {

        super(props);
        this.state = {
            ...props.receiveData.data,
            visibleForm: false
        };
    }

    setActiveStatus = (value) => {
        this.setState({visibleForm: value});
        console.log("card info: ",this.state);
    }

    render() {
        const { visibleForm } = this.state;
        const {setActiveStatus} = this;
        const individualCardName = "main-card"+this.props.cardIndex;

        return(
            <div className={"main-card" + " " + individualCardName}>

                { this.state.visibleForm ? (
                    <div
                        style={{
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'center',
                            flexDirection: 'column'
                        }}
                    >
                        <EditForm operationType="edit" setActiveStatus={setActiveStatus}/>
                    </div>

                    ) : (
                        <div>
                            <h3 className="main-card__title">
                                {this.state?.name}
                                {this.state?.age}
                                year
                            </h3>
                            <p className="main-card__text">{this.state?.email}</p>
                            <p className="main-card__text">{this.state?.phone}</p>
                        </div>
                    )
                }


                <div className="button-group">
                    <button
                        onClick={
                            () => !visibleForm && setActiveStatus({visibleForm: true})
                        }
                        style={{margin:".125em"}} >edit</button>
                    <button
                        onClick={
                            ()=>{
                                this.props.cardDelete({idCard: this.props.receiveData._id ,nameCard: individualCardName})
                            }
                        }
                        style={{margin:".125em"}}>X</button>
                </div>
            </div>
        )
    }
}