import React, {Component} from 'react';
import MainCard from '../MainCard/MainCard'
import AddCard from "../AddCard/AddCard";

const API = 'http://178.128.196.163:3000'

let DataTableStyle = {
    display: "Flex",
    flexDirection: "column"
    // flexWrap: "wrap"
};

export default class DataTable extends Component {

    constructor(props) {
        super(props);
        this.state = {
            cardList: []
        };
    }

    deleteCard = (value) => {

        fetch(API + "/api/records/:" + value.idCard,{method: 'DELETE'})
            .then(result=>console.log("test delete ",result))
            .then((response)=>{ if(response.ok){document.querySelector("." + value.nameCard).remove()}  })
            .catch(error => console.log("not delete on server ", error));
        // console.log(this.props.receiveData._id);


    }

    componentDidMount() {
        fetch(API + '/api/records')
            .then(response => response.json())
            .then(data => {
                this.setState({
                    cardList: data
                });
            })
            .catch(error => console.log('error my request', error)); // этот выполнится в случае ошибки
    }

    render() {
        return (
            <div style={DataTableStyle} className="data-table">
                <AddCard API={API} />

                {this.state.cardList.map(
                    (card, index) => {
                        console.log("transfer in card" ,card)
                        return <MainCard
                            receiveData={card}
                            cardIndex={index}
                            API={API}
                            cardDelete={this.deleteCard}
                        />

                    }
                )}



            </div>
        )
    }
}