import React, { Component } from 'react';
import EditForm from "../EditFormComponent/EditForm";

export default class AddCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            visibleForm: false
        };
    }

    setActiveStatus = (value) => {
        this.setState({visibleForm: value})
    }

    render() {
        const { visibleForm } = this.state; //деструктуризация
        const {setActiveStatus} = this;

        return(
            <div
                onClick={() => !visibleForm && setActiveStatus({visibleForm: true})}
                className="main-card"
                style={{
                     display: 'flex',
                     justifyContent: 'center',
                     alignItems: 'center',
                     flexDirection: 'column'
                 }}>
                    {this.state.visibleForm ? (
                        <EditForm API={this.props.API} operationType="add" setActiveStatus={setActiveStatus} />
                    ) : (
                        <h3 className="main-card__title">Добавить элемент</h3>
                    )}
            </div>
        )
    }
}



